module Main where

import           Leif             (money)
import           Test.Tasty
import           Test.Tasty.HUnit

main :: IO ()
main = defaultMain suite

suite :: TestTree
suite = testGroup "Unit Tests"
  [ testCase "Is the money good?" $ money @?= 10000
  ]
